import * as functions from 'firebase-functions'
import * as firebaseAdmin from "firebase-admin"
import * as distance from "gps-distance"

const {PubSub} = require('@google-cloud/pubsub');

// import { peopleInterface } from './model'
import { pubsubConfig } from './config'

firebaseAdmin.initializeApp(functions.config().firebase)
const pubsub = new PubSub(pubsubConfig.projectId);

exports.googleHomeTrigger = functions.firestore.document('personal-accounts/ui9nr5vUoQPnQuPZj6sou7fSial2/peoples/{MAC_address}')
    .onUpdate(async (change, context) => {
        
        const beforeData = change.before.data()
        const afterData = change.after.data() 
        console.log("****: " + JSON.stringify(afterData))
        // const firebaseId = context.params.firebaseId
        // const MAC_address = context.params.MAC_address        
        // const moudule = getLocation(afterData)

        if(afterData.device=="bracelet"){
        const physiologicalState = verifyPhysiologicalState(beforeData, afterData)
        await notifyPhysiological(physiologicalState, afterData)
        }
    // .catch(err =>console.log("***error: "+err))
    });

const verifyPhysiologicalState = (beforeData, afterData): number => {
    let physiologicalState

    const beforePhysiological = beforeData.physiological
    const afterPhysiological = afterData.physiological
    const beforeCoachMessage = beforeData.coachMessage
    const afterCoachMessage = afterData.coachMessage
    const afterThing = afterData.thing
    
    if (afterPhysiological.HR != beforePhysiological.HR) {
        //心律
        if(afterThing.Status = ""){
            if(afterPhysiological.HR >= 100 || afterPhysiological.HR <= 60){
                physiologicalState = 1
            }
        }
        else if(afterThing.Status = "fall"){
            if(afterPhysiological.HR >= 100 || afterPhysiological.HR <= 60){
                physiologicalState = 101
            }
        }
        else if(afterThing.Status = "fallAlert"){
            if(afterPhysiological.HR >= 100 || afterPhysiological.HR <= 60){
                physiologicalState = 102
            }
        }else{
            if(afterPhysiological.HR >= 100 || afterPhysiological.HR <= 60){
                physiologicalState = 1
            }
        }
        
    } else if (afterPhysiological?.HRV != beforePhysiological?.HRV) {
        //心律變異率
        physiologicalState = 2

    } else if (afterPhysiological?.OBLA != beforePhysiological?.OBLA) {
        //乳酸
        physiologicalState = 3

    } else if (afterPhysiological?.SpO2 != beforePhysiological?.SpO2) {
        //血氧
        physiologicalState = 4

    } else if (afterPhysiological?.weight != beforePhysiological?.weight) {
        //體重
        physiologicalState = 5

    } else if (afterPhysiological?.BP != beforePhysiological?.BP) {
        //血壓
        physiologicalState = 6

    } else if (afterPhysiological?.BS != beforePhysiological?.BS) {
        //血糖
        physiologicalState = 7

    } else if (beforeData.coachMessage != afterData.coachMessage) {
        //血糖
        physiologicalState = 8

    } else {
        //生理數據不變
        physiologicalState = 0

    }
    return physiologicalState
}

const notifyPhysiological = async (physiologicalState: number, afterData): Promise<any> => {
    let notifyMessage
    // let location
    //         location= [Number(afterData.geoPoint.latitude),Number(afterData.geoPoint.longitude)]
    //         let arr = []
    //     await firebaseAdmin.firestore().collection('personal-accounts').doc('ui9nr5vUoQPnQuPZj6sou7fSial2').collection('googleHome').get()
    //     .then(querySnapshot=>{
    //         querySnapshot.forEach(doc=>{
    //             let data ={
    //                 geoPoint: doc.data().geoPoint,
    //                 ip: doc.data().ip,
    //                 name: doc.data().name,
    //                 place: doc.data().place,
    //                 type: doc.data().type,
    //                 distance:distance(location[0],location[1],doc.data().geoPoint.latitude,doc.data().geoPoint.longitude)*1000
    //             }
    //             arr.push(data)
    //         })
    //     })
        
        
    //     arr = arr.sort(function (a, b) {
    //         return a.distance-b.distance;
    //     });
    let arr=[];
    let data
    switch(afterData.name){
        case "瑋航" :case "育銘":case "柏諺":case "明儒":case "昱三":case "詠翔":case "敬平":case "張力穎":case "柏諺":
            data={
                geoPoint: 0,
                ip: '192.168.1.109',
                name: 'googleNest',
                place: '709-3',
                type:  'googleNest',
                distance:0
            }
        break
        case "昱彬" :case "白金瀚":case "舜民":case "罡睿":case "耀瑋":case "小菜":case "傳宗":
            data={
                geoPoint: 0,
                ip: '192.168.0.21',
                name: 'googleHome',
                place: '709-2',
                type: 'googleHome',
                distance:0
            }
        break
        default:
            data={
                geoPoint: 0,
                ip: '192.168.1.108',
                name: '',
                place: '709-3',
                type: '709-3-google-home',
                distance:0
            }
        break
    }
   
    let time='*'+String(new Date().toISOString().slice(0,10));
    arr.push(data)
    switch (physiologicalState) {
            case 0:
                return
            case 1:

                if(afterData.physiological?.HR===0){
                    notifyMessage = {
                        MAC_address:afterData.MAC_address,
                        name:afterData.name, 
                        result:afterData.physiological?.HR,
                        message:`測試:${afterData.name}，請戴上手環喔!`,
                        time:time,
                        moudule:arr[0]
                    }
                }else{
                    notifyMessage = {
                        MAC_address:afterData.MAC_address,
                        name:afterData.name, 
                        result:afterData.physiological?.HR,
                        message:`${afterData.name}目前心跳每分鐘${afterData.physiological?.HR}下`,
                        time:time,
                        moudule:arr[0]
                    }
                }

                break;
            case 2:
                // notifyMessage = `${afterData.name}目前心跳變化率${afterData.physiological?.HRV}`
                notifyMessage = {
                    MAC_address:afterData.MAC_address,
                    name:afterData.name, 
                    result:afterData.physiological?.HR,
                    message:`${afterData.name}目前心跳每分鐘${afterData.physiological?.HR}下`,
                    time:time,
                    moudule:arr[0]
                }
                break;
            case 3:
                // notifyMessage = `${afterData.name}目前乳酸指數為${afterData.physiological?.OBLA}`
                notifyMessage = {
                    MAC_address:afterData.MAC_address,
                    name:afterData.name, 
                    result:afterData.physiological?.OBLA,
                    message:`${afterData.name}目前乳酸指數為${afterData.physiological?.OBLA}`,
                    time:time,
                    moudule:arr[0]
                }
                break;
            case 4:
                // notifyMessage = `${afterData.name}目前血氧濃度為${afterData.physiological?.SpO2}`
                notifyMessage = {
                    MAC_address:afterData.MAC_address,
                    name:afterData.name, 
                    result:afterData.physiological?.SpO2,
                    message:`${afterData.name}目前血氧濃度為${afterData.physiological?.SpO2}`,
                    time:time,
                    moudule:arr[0]
                }
                break;
            case 5:
                // notifyMessage = `${afterData.name}體重為${afterData.physiological?.weight}公斤`
                notifyMessage = {
                    MAC_address:afterData.MAC_address,
                    name:afterData.name, 
                    result:afterData.physiological?.weight,
                    message:`${afterData.name}體重為${afterData.physiological?.weight}公斤`,
                    time:time,
                    moudule:arr[0]
                }
                break;
            case 6:
                // notifyMessage = `${afterData.name}血壓為${afterData.physiological?.BP}`
                notifyMessage = {
                    MAC_address:afterData.MAC_address,
                    name:afterData.name, 
                    result:afterData.physiological?.BP,
                    message:`${afterData.name}血壓為${afterData.physiological?.BP}`,
                    time:time,
                    moudule:arr[0]
                }
                break;
            case 7:
                // notifyMessage = `${afterData.name}血糖為${afterData.physiological?.BS}`
                notifyMessage = {
                    MAC_address:afterData.MAC_address,
                    name:afterData.name, 
                    result:afterData.physiological?.BS,
                    message:`${afterData.name}血糖為${afterData.physiological?.BS}`,
                    time:time,
                    moudule:arr[0]
                }
                break;
            case 8:
                    // notifyMessage = `${afterData.name}血糖為${afterData.physiological?.BS}`
                notifyMessage = {
                    MAC_address:afterData.MAC_address,
                    name:afterData.name, 
                    result:`${afterData.coachMessage}`,
                    message:`${afterData.coachMessage}`,
                    time:time,
                    moudule:arr[0]
                }
                break;
            case 101:
                if(afterData.physiological?.HR===0){
                    notifyMessage = {
                        MAC_address:afterData.MAC_address,
                        name:afterData.name, 
                        result:afterData.physiological?.HR,
                        message:`危急狀況通知，${afterData.name}，沒有心跳了。疑似跌倒在${afterData.thing.room}的${afterData.thing.place}附近`,
                        time:time,
                        moudule:arr[0]
                    }
                }else{
                    notifyMessage = {
                        MAC_address:afterData.MAC_address,
                            name:afterData.name, 
                            result:afterData.physiological?.HR,
                            message:`${afterData.name},剛剛在走動，心跳${afterData.physiological?.HR}下。疑似跌倒在${afterData.thing.room}的${afterData.thing.place}附近`,
                            time:time,
                            moudule:arr[0]
                        }
                    }
                break;
            case 102:
                if(afterData.physiological?.HR===0){
                    notifyMessage = {
                        MAC_address:afterData.MAC_address,
                        name:afterData.name, 
                        result:afterData.physiological?.HR,
                        message:`危急狀況通知，${afterData.name}，沒有心跳了。疑似跌倒在${afterData.thing.room}的${afterData.thing.place}附近`,
                        time:time,
                        moudule:arr[0]
                    }
                }else{
                    notifyMessage = {
                        MAC_address:afterData.MAC_address,
                            name:afterData.name, 
                            result:afterData.physiological?.HR,
                            message:`危急狀況通知，${afterData.name}，心跳${afterData.physiological?.HR}下。疑似跌倒在${afterData.thing.room}的${afterData.thing.place}附近`,
                            time:time,
                            moudule:arr[0]
                        }
                    }
                break;
            default:
                break;
        }
                //如果pub object要先轉字串在轉Buffer
    await recordMessage(notifyMessage)
    const dataBuffer = Buffer.from(JSON.stringify(notifyMessage));
                //for string
                // const dataBuffer = Buffer.from(notifyMessage);
    const messageId = await pubsub.topic(pubsubConfig.topicName).publish(dataBuffer);   
    
    console.log(`**Message: ${messageId} published.`+`notifyMessage: `+notifyMessage );
    // if(arr[0].distance<10){
    //     //如果pub object要先轉字串在轉Buffer
    //     const dataBuffer = Buffer.from(JSON.stringify(notifyMessage));
    //     //for string
    //     // const dataBuffer = Buffer.from(notifyMessage);
    //     const messageId = await pubsub.topic(pubsubConfig.topicName).publish(dataBuffer);
    //     console.log(`**Message: ${messageId} published.`+`notifyMessage: `+notifyMessage );
    // }
    // else{
    //     console.log('Patient is not here')
    // }
}

async function recordMessage(message){

    let time="N"+message.time
    await firebaseAdmin.firestore().collection('personal-accounts').doc('ui9nr5vUoQPnQuPZj6sou7fSial2').collection('notifyTest').doc(message.MAC_address).collection(time).add(message)

}


