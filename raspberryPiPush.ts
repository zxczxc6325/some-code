// const http = require('http')
const googleHome = require('./ghp');
const { PubSub } = require('@google-cloud/pubsub');
// const config = require('./config')
const axios = require('axios')

  const language = "zh-TW"
  const accent = "zh-TW"
  const options = {
    language: language,
    accent: accent
  }
  
  const apiUrl='apiUrl'
  
  const subscriptionName = 'utl-smart-speakers'
  
  // function listenForMessages() {
  const pubsub = new PubSub({
    projectId: '',
    //UTL Key 金曜千萬不要上傳！！！！！！！！
    keyFilename: 'UTLKey.json'
  });
  
  // References an existing subscription
  const maxInProgress = 1
  const subscriberOptions = {
    flowControl: {
      maxMessages: maxInProgress
    }
  }
  const subscription = pubsub.subscription(subscriptionName,subscriberOptions);
  
  // Listen for new messages until timeout is hit
  subscription.on('message',(message) => {
  
    // console.log(message.data)
    console.log(Buffer.from(message.data, 'base64').toString())
    // const notifyMessage =  Buffer.from(message.data, 'base64').toString()
     const notifyMessage =JSON.parse(Buffer.from(message.data, 'base64').toString())
      const googleHomeIp = "192.168.72.200"
      // const googleHomeIp = notifyMessage.moudule.ip
    pushMessage(notifyMessage.message,options.language,googleHomeIp)
    message.ack();
  })
  subscription.on('error', function (error) {
    // Do something with the error
    console.error(`ERROR: ${error}`);
    throw error;
  });
  async function pushMessage(message,language,ip){
    console.log('start')
    const googleHomeIp =ip
    const myHome = new googleHome(googleHomeIp, options)
    let result
    await axios.post(apiUrl, {
        voice:{
          "languageCode":language
        },
        input:{
            "text":message
        },
        audioConfig:{
            "audioEncoding":"mp3"
        }
      })
      .then(res => {
        result=res.data.audioContent
      })
      .catch(error => {
        console.log('++error')
        console.error(error)
      })
      myHome.push('data:audio/mpeg;base64,'+result)
  }
